# Q
A very simple queue which handles multithreading.

## Dependencies

Standard C++ libraries should be fine enough. Requires C++11 and above

## License

This uses the following MiT License.

```
MIT License

Copyright (c) 2018 Justin Huang

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

## Usage

Very simple functionality. Quick example:

```
#include "q.hpp"

#include <chrono>
#include <ctime>
#include <iostream>
#include <thread>

bool dead = false;

void worker(Queue<int>* q)
{
    for(int i = 0; i < 15; i++)
    {
        q->Put(std::time(nullptr));
        {
            using namespace std::literals::chrono_literals;
            std::this_thread::sleep_for(500ms);
        }
    }
}

void listener(Queue<int>* q)
{
    while(!dead || !q->Empty())
    {
        try{
            std::cout << q->Get(false) << std::endl;
        } catch (std::exception) {
        {
            using namespace std::literals::chrono_literals;
            std::this_thread::sleep_for(500ms);
        }
        }
    }
}


int main()
{
    Queue<int> q;


    std::thread t1(&worker, &q);
    std::thread t2(&worker, &q);
    std::thread t3(&worker, &q);
    std::thread t4(&worker, &q);
    std::thread t5(&worker, &q);


    std::thread t6(&listener, &q);

    t1.join();
    t2.join();
    t3.join();
    t4.join();
    t5.join();

    dead = true;
    t6.join();
}
```
